FROM python:3.7-slim-stretch

LABEL maintainer="kuzmich@ider.xyz"

COPY server.py /app/
COPY requirements.txt /app/

RUN pip install -r /app/requirements.txt

EXPOSE 8000

ENTRYPOINT ["python", "/app/server.py"]